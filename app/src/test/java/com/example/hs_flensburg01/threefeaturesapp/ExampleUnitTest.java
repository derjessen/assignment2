package com.example.hs_flensburg01.threefeaturesapp;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {

    ThreeFeatures features = new ThreeFeatures();

    @Test
    public void createUserTest() throws Exception {

        String validName = "Mo";
        String validPassword = "hsflensburg";

        String nameNoUpperCase = "mo";
        String nameTooLong = "ThisNameIsTooLong";
        String passwordNULL = null;

        assertEquals(true, features.createUser(validName, validPassword));
        assertEquals(false, features.createUser(nameNoUpperCase, validPassword));
        assertEquals(false, features.createUser(nameTooLong, validPassword));
        assertEquals(false, features.createUser(validName, passwordNULL));
        assertEquals(false, features.createUser(nameNoUpperCase, passwordNULL));
        assertEquals(false, features.createUser(nameTooLong, passwordNULL));
    }

    @Test
    public void loginTest() throws Exception {

        String validName = "Torben";
        String validPassword = "hsflensburg";

        String inValidName = "awe";
        String inValidPassword = "some";

        assertEquals(true, features.login(validName, validPassword));
        assertEquals(false, features.login(inValidName, inValidPassword));

    }

    @Test
    public void divideTwoNumbersTest() throws Exception {

        try {
            features.divideTwoNumbers(4, 0);
            fail("Klappt");
        } catch(ArithmeticException e) {
            e.printStackTrace();
        }

        assertEquals(10, features.divideTwoNumbers(20, 2));
    }
}