package com.example.hs_flensburg01.threefeaturesapp;

/**
 * Created by hs-flensburg01 on 06/10/16.
 */
public class ThreeFeatures {

    private String userName = "Torben";
    private String userPassword = "hsflensburg";

    public Boolean createUser(String name, String password){

        boolean isUserValid = false;

        if((Character.isUpperCase(name.charAt(0))) && (name.length() <= 10)){
            if(password != null)
                isUserValid = true;
        }

        return isUserValid;
    }

    public Boolean login(String name, String password) {

        Boolean valid = false;
        // Check Username
        if(name.equals(userName) && password.equals(userPassword)){
            valid = true;
        }else{
            valid = false;
        }
        return valid;
    }

    public int divideTwoNumbers(int i, int i1) {
        return i/i1;
    }
}
