package com.example.hs_flensburg01.threefeaturesapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ThreeFeaturesApp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_three_features_app);
    }
}
